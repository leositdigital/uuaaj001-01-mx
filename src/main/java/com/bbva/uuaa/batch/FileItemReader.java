package com.bbva.uuaa.batch;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class FileItemReader implements FieldSetMapper<String>{//AccountDTO

	@Override
	public String mapFieldSet(FieldSet rowFile) throws BindException {
		String numCuenta = rowFile.readString("NUMCUENTA");
		
		String result = numCuenta +""+"";
		
		return result;
	}

}
